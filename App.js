import React, {Component} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
import CalcBtn from "./src/components/CalcBtn";

class App extends Component {
  state = {
    expression: '',
    isResult: true
  };

  buttonPressedHandler = btnName => {
    const expression = (this.state.expression === 'error' ||
      this.state.expression === 'Infinity' ||
      (this.state.isResult && !isNaN(btnName))) ?
      btnName : this.state.expression + btnName;
    this.setState({expression, isResult: false});
  };

  calculateHandler = () => {
    if (!this.state.expression) return null;
    let result;
    try {
      result = String(eval(this.state.expression));
    } catch(e) {
      result = 'error';
    }
    this.setState({expression: result, isResult: true});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.expressionContainer}>
          <TextInput
            style={styles.input}
            value={this.state.expression}
          />
        </View>
        <View style={styles.buttons}>
          <CalcBtn addClass="btnNumber" name="7" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="8" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="9" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnOperation" name="/" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="4" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="5" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="6" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnOperation" name="*" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="1" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="2" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="3" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnOperation" name="-" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="." buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnNumber" name="0" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnOperation" name="+" buttonPressed={this.buttonPressedHandler} />
          <CalcBtn addClass="btnResult" name="=" buttonPressed={this.calculateHandler} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 30
  },
  input: {
    width: '100%',
    height: 40,
    fontSize: 20,
    textAlign: 'right'
  },
  expressionContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  buttons: {
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'flex-start'
  }
});

export default App;