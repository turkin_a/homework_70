import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from "react-native";

const CalcBtn = props => (
  <TouchableOpacity onPress={() => props.buttonPressed(props.name)}>
    <View style={[styles.btn, styles[props.addClass]]}>
      <Text style={styles.btnText}>{props.name}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  btn: {
    padding: 20,
    margin: 2
  },
  btnNumber: {
    backgroundColor: 'lightblue'
  },
  btnOperation: {
    backgroundColor: 'yellow'
  },
  btnResult: {
    backgroundColor: 'blue'
  },
  btnClear: {
    backgroundColor: 'red'
  },
  btnText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    width: 30
  }
});

export default CalcBtn;